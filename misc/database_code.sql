CREATE DATABASE IF NOT EXISTS `comunicationltd` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `comunicationltd`;

CREATE TABLE IF NOT EXISTS `accounts` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(50) NOT NULL,
	`password` varchar(255) NOT NULL,
	`email` varchar(100) NOT NULL,
    `password_hist_1` varchar(255) NOT NULL DEFAULT '',
    `password_hist_2` varchar(255) NOT NULL DEFAULT '',
    `password_hist_3` varchar(255) NOT NULL DEFAULT '',
    `salt` varchar(255) NOT NULL DEFAULT '',
    `salt_hist_1` varchar(255) NOT NULL DEFAULT '',
    `salt_hist_2` varchar(255) NOT NULL DEFAULT '',
    `salt_hist_3` varchar(255) NOT NULL DEFAULT '',
    `login_attempts` varchar(50) NOT NULL,
    `password_reset_code` varchar(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `costumers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`costumername` varchar(50) NOT NULL,
	`costumeremail` varchar(100) NOT NULL,
    `registeredby` varchar(100) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

select * from  accounts;
select * from  costumers;