import smtplib
from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
from flask_mail import Mail, Message
import MySQLdb.cursors
import re
import binascii
import os
import hashlib
import html

import ConfigurationModule as PasswordManager

import ssl
context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
context.load_cert_chain('ssl_certificate/cert.pem',
                        'ssl_certificate/key.pem')

app = Flask(__name__)

# Change this to your secret key (can be anything, it's for extra protection)
app.secret_key = 'Aa123456~'

# Enter your database connection details below
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'Admin'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'comunicationltd'

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'compsecproj2020@gmail.com'
app.config['MAIL_PASSWORD'] = 'Project1212~'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

# Intialize MySQL
mysql = MySQL(app)

mail = Mail(app)

################################################################################################################################
################################################################################################################################
################################################################################################################################


@app.route('/pythonlogin/', methods=['GET', 'POST'])
def login():
    # Output message if something goes wrong...
    msg = ''

    password_conf = PasswordManager.read_conf("conf")

    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:

        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        
        cursor.execute('SELECT * FROM accounts WHERE username = %s ', (username,))  # TEMP#
        # Fetch one record and return result
        account = cursor.fetchone()
        # If account exists in accounts table in out database
        if account:
            login_attempts = account['login_attempts']
            salt_byte = account['salt'].encode()  # TEMP#
            hash = hashlib.pbkdf2_hmac(
                'sha1', password.encode(), salt_byte, 100000)  # TEMP#
            password = binascii.hexlify(hash)  # TEMP#
            cursor.execute(
                'SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
            account = cursor.fetchone()
        else:
            # Account doesnt exist or username/password incorrect
            msg = 'Username doesnt exist!'
            return render_template('index.html', msg=msg)

        if(int(login_attempts) >= password_conf["loginAttempts"]):
            msg = 'Account blocked and now must be resetted!'
            return render_template('index.html', msg=msg)

        # Check again but now with the decoded password
        if account:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
            # Redirect to home page
            return redirect(url_for('home'))
        else:
            # Account doesnt exist or username/password incorrect
            cursor.execute('UPDATE accounts SET login_attempts = %s WHERE username = %s',
                           (int(login_attempts) + 1, username))
            mysql.connection.commit()
            msg = 'Incorrect password!'

    # Show the login form with message (if any)
    return render_template('index.html', msg=msg)

################################################################################################################################
################################################################################################################################
################################################################################################################################


@app.route('/pythonlogin/reset', methods=['GET', 'POST'])
def reset():
    msg = ''
    if request.method == 'POST' and 'email' in request.form:
        email = request.form['email']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE email = %s', (email,))
        account = cursor.fetchone()
        if account:
            hash = hashlib.pbkdf2_hmac(
                'sha1', email.encode(), account['salt'].encode(), 10000)  # TEMP#
            reset_code = binascii.hexlify(hash)  # TEMP#
            cursor.execute(
                'UPDATE accounts SET password_reset_code = %s WHERE email = %s', (reset_code, email,))
            mysql.connection.commit()
            email_info = Message(
                'Password Reset', sender='compsecproj2020@gmail.com', recipients=[email])
            email_info.body = 'Copy this code to reset your password\n' + \
                reset_code.decode('utf-8')
            mail.send(email_info)
            return render_template('resetcode.html/', email=email, msg="Reset code has been sent to your email")
        else:
            msg = 'This email address does not belong to any account!'
    return render_template('reset.html', msg=msg)

################################################################################################################################
################################################################################################################################
################################################################################################################################


@app.route('/pythonlogin/resetcode/<string:email>', methods=['GET', 'POST'])
def resetcode(email):
    if request.method == 'POST' and 'resetcode' in request.form:
        reset_code = request.form['resetcode']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(
            'SELECT * FROM accounts WHERE email = %s AND password_reset_code = %s', (email, reset_code))
        account = cursor.fetchone()

        if account == None:
            return render_template('resetcode.html/', email=email, msg="Wrong reset code")

    return render_template('changepassword.html/', email=email, msg="")

################################################################################################################################
################################################################################################################################
################################################################################################################################

def get_hashed_password(password, salt):
    hash = hashlib.pbkdf2_hmac(
        'sha1', password.encode(), salt.encode(), 100000)
    password = binascii.hexlify(hash)

    return password

################################################################################################################################
################################################################################################################################
################################################################################################################################

def check_password_history(password_conf, account, new_password):
    DB_PASSWORD_PREFIX = 'password_hist_'
    DB_SALT_PREFIX = 'salt_hist_'
    history = password_conf["history"]

    hashed_history = get_hashed_password(new_password, account['salt'])

    if (account['password'] == hashed_history):
        return True

    for i in range(1, history+1):
        pass_field_name = DB_PASSWORD_PREFIX + str(i)
        salt_field_name = DB_SALT_PREFIX + str(i)

        hashed_history = get_hashed_password(
            new_password, account[salt_field_name])

        if account[pass_field_name].encode() == hashed_history:
            return True

    return False

################################################################################################################################
################################################################################################################################
################################################################################################################################

@app.route('/pythonlogin/changepassword/<string:email>', methods=['GET', 'POST'])
def changepassword(email):
    if request.method == 'POST' and 'new_password' in request.form and 'new_password_repeat' in request.form:
        new_password = request.form['new_password']
        new_password_repeat = request.form['new_password_repeat']

        if new_password != new_password_repeat or new_password == "":
            return render_template('changepassword.html/', email=email, msg="Passwords do not match")

        password_conf = PasswordManager.read_conf("conf")

        if(len(new_password) < password_conf["passwordLength"]):
            return render_template('changepassword.html/', email=email, msg="password too short!")

        if(not PasswordManager.check_password(new_password)):
            return render_template('changepassword.html/', email=email, msg="weak password!!")

        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(
            'SELECT * FROM accounts WHERE email = %s AND email = %s', (email, email))
        account = cursor.fetchone()

        if account:
            salt = binascii.hexlify(os.urandom(16))
            hash = hashlib.pbkdf2_hmac(
                'sha1', new_password.encode(), salt, 100000)
            new_password = binascii.hexlify(hash)

            if(check_password_history(password_conf, account, request.form['new_password'])):
                return render_template('changepassword.html/', email=email, msg="password was in use!!")

            pass_hist_1 = account["password"]
            pass_hist_2 = account["password_hist_1"]
            pass_hist_3 = account["password_hist_2"]

            salt_hist_1 = account["salt"]
            salt_hist_2 = account["salt_hist_1"]
            salt_hist_3 = account["salt_hist_2"]

            cursor.execute('UPDATE accounts SET password = %s, salt = %s, password_hist_1 = %s, password_hist_2 = %s, password_hist_3 = %s, salt_hist_1 = %s, salt_hist_2 = %s, salt_hist_3 = %s WHERE id = %s',
                           (new_password, salt, pass_hist_1, pass_hist_2, pass_hist_3, salt_hist_1, salt_hist_2, salt_hist_3, account['id']))
            mysql.connection.commit()
            return render_template('index.html', msg="Password changed")

    return ''

################################################################################################################################
################################################################################################################################
################################################################################################################################

# http://localhost:5000/pythinlogin/profile - this will be the profile page, only accessible for loggedin users
@app.route('/pythonlogin/profile', methods=['GET', 'POST'])
def profile():

    msg = ""

    # Check if user is loggedin
    if 'loggedin' in session:

        if request.method == 'POST' and 'password' in request.form:
            password = request.form['password']
            username = session['username']

            # Check if account exists using MySQL
            cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute('SELECT * FROM accounts WHERE username = %s ', (username,))  # TEMP#
            # Fetch one record and return result
            account = cursor.fetchone()
            # If account exists in accounts table in out database
            if account:
                salt_byte = account['salt'].encode()  # TEMP#
                hash = hashlib.pbkdf2_hmac('sha1', password.encode(), salt_byte, 100000)  # TEMP#
                password = binascii.hexlify(hash)  # TEMP#
                cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
                account = cursor.fetchone()
                if account:
                    return render_template('changepassword.html', email=account['email'])
                else:
                    msg = "Wrong Password"

        # We need all the account info for the user so we can display it on the profile page
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE id = %s', (session['id'],))
        account = cursor.fetchone()
        # Show the profile page with account info
        return render_template('profile.html', account=account, msg=msg)
    # User is not loggedin redirect to login page
    return redirect(url_for('login'))

################################################################################################################################
################################################################################################################################
################################################################################################################################

# http://localhost:5000/pythinlogin/home - this will be the home page, only accessible for loggedin users


@app.route('/pythonlogin/home')
def home():
    # Check if user is loggedin
    if 'loggedin' in session:
        # User is loggedin show them the home page
        return render_template('home.html', username=session['username'])
    # User is not loggedin redirect to login page
    return redirect(url_for('login'))

################################################################################################################################
################################################################################################################################
################################################################################################################################

# http://localhost:5000/pythinlogin/register - this will be the registration page, we need to use both GET and POST requests


@app.route('/pythonlogin/register', methods=['GET', 'POST'])
def register():
    # Output message if something goes wrong...
    msg = ''
    salt = os.urandom(16)  # TEST#
    salt_byte = binascii.hexlify(salt)  # TEST#
    # Check if "username", "password" and "email" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:

        password = request.form['password']
        password_conf = PasswordManager.read_conf("conf")

        if(len(password) < password_conf["passwordLength"]):
            msg = 'password too short!'
            return render_template('register.html', msg=msg)

        if(not PasswordManager.check_password(password)):
            msg = 'weak password!!'
            return render_template('register.html', msg=msg)

        # Create variables for easy access
        username = request.form['username']
        email = request.form['email']  # TEST#
        hash = hashlib.pbkdf2_hmac('sha1', password.encode(), salt_byte, 100000)  # TEST#
        attempts_to_login = '0'  # TEST#
        password = binascii.hexlify(hash)
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

        cursor.execute('SELECT * FROM accounts WHERE email = %s', (email,))
        account = cursor.fetchone()
        if account:
            msg = 'Email already belongs to another user!'
            return render_template('register.html', msg=msg)

        cursor.execute(
            'SELECT * FROM accounts WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            msg = 'Username already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'Username must contain only characters and numbers!'
        elif not username or not password or not email:
            msg = 'Please fill out the form!'
        else:
            # Account doesnt exists and the form data is valid, now insert new account into accounts table
            cursor.execute('INSERT INTO accounts (username, password, email, login_attempts, password_hist_1, password_hist_2, password_hist_3, salt, salt_hist_1 , salt_hist_2, salt_hist_3) VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                           (username, password, email, attempts_to_login, password, password, password, salt_byte, salt_byte, salt_byte, salt_byte))
            mysql.connection.commit()
            msg = 'You have successfully registered!'
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    # Show registration form with message (if any)
    return render_template('register.html', msg=msg)

################################################################################################################################
################################################################################################################################
################################################################################################################################

# http://localhost:5000/python/logout - this will be the logout page


@app.route('/pythonlogin/logout')
def logout():
    # Remove session data, this will log the user out
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    # Redirect to login page
    return redirect(url_for('login'))


################################################################################################################################
################################################################################################################################
################################################################################################################################


@app.route('/pythonlogin/addcostumers', methods=['GET', 'POST'])
def addcostumers():

    msg = ""

    if request.method == 'POST' and 'costumername' in request.form and 'costumeremail' in request.form:

        # NOTE: REMOVE "html.escape" TO ALLOW XSS-STORAGE ATTACK
        costumer_name = html.escape(request.form['costumername'])
        costumer_email = html.escape(request.form['costumeremail'])

        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)

        cursor.execute('INSERT INTO costumers (costumername, costumeremail, registeredby) VALUES (%s, %s, %s)',
                       (costumer_name, costumer_email, session['username']))
        mysql.connection.commit()

        msg = "Costumer Added!"

        cursor.execute(
            "select * from costumers where Id=(SELECT LAST_INSERT_ID())")
        costumer = cursor.fetchone()
        msg = "Costumer {} Added!".format(costumer['costumername'])

    return render_template('addcostumers.html', msg=msg)


################################################################################################################################
################################################################################################################################
################################################################################################################################


@app.route('/pythonlogin/manage', methods=['GET', 'POST'])
def manage():

    msg = ""
    
    new_password_conf = {}
    password_content_list = []

    if(request.method == 'POST' and 'passwordlegnth' in request.form 
    and 'passwordhistory' in request.form 
    and 'loginattempts' in request.form):

        if(int(request.form['passwordlegnth']) < 15):
            new_password_conf["passwordLength"] = int(request.form['passwordlegnth'])
        else:
            new_password_conf["passwordLength"] = 15

        if(int(request.form['passwordhistory']) < 3):
            new_password_conf["history"] = int(request.form['passwordhistory'])
        else:
            new_password_conf["history"] = 3

        if(int(request.form['loginattempts']) > 1):
            new_password_conf["loginAttempts"] = int(request.form['loginattempts'])
        else:
            new_password_conf["loginAttempts"] = 1

        if('dictionaryDenial' in request.form ):
            new_password_conf["dictionaryDenial"] = "True"
        else:
            new_password_conf["dictionaryDenial"] = "False"

        if('p_AZ' in request.form ):
            password_content_list.append("A-Z")

        if('p_az' in request.form ):
            password_content_list.append("a-z")

        if('p_09' in request.form ):
            password_content_list.append("0-9")

        if('p_sym' in request.form ):
            password_content_list.append("@~")

        if(len(password_content_list) is 0):
            new_password_conf["passwordContent"] = ["0-9"]
        else:
            new_password_conf["passwordContent"] = password_content_list

        PasswordManager.save_conf("conf", new_password_conf)

    return render_template('manage.html', msg=msg)

if __name__ == '__main__':
    app.run(ssl_context=context)
