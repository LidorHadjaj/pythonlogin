import json
import re

bad_passwords = ["123456",
                 "ABCDEF",
                 "abcdef"]

def read_conf(file_name):
    try:
        with open(file_name) as json_file:
            data = json.load(json_file)
            return data
    except Exception as inst:
        # default value in case file open fails
        print(inst)
        return{
            "passwordLength": 10,
            "passwordContent": ["A-Z", "a-z", "0-9", ".,@~"],
            "history": 3,
            "loginAttempts": 3,
            "dictionaryDenial": False
        }

    finally:
        json_file.close()


def save_conf(file_name, data):
    try:
        with open(file_name, 'wt') as json_file:
            json.dump(data, json_file)

    except:
        raise Exception("couldn't write to file")

    finally:
        json_file.close()


def check_password(password):
    configuration = read_conf("conf")

    # Make sure the password isnt in the dict if
    # the dict deinal is false
    if(configuration['dictionaryDenial'] == "False" and password in bad_passwords):
        return False


    password_regex_p1 = ('').join('(?=.*[{0}])'.format(w) for w in configuration["passwordContent"])
    password_regex_p2 = '([' + ('').join(configuration["passwordContent"]) + '])+'

    full_regex = password_regex_p1 + password_regex_p2

    return re.fullmatch(full_regex, password) is not None


